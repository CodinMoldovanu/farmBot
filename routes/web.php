<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/config', 'HomeController@config')->name('config')->middleware('auth');
Route::get('/api/home', 'HomeController@homeAPI')->name('homeAPI')->middleware('auth');
Route::post('/config/save', 'HomeController@saveConfig')->name('saveConfig')->middleware('auth');
Route::get('/config/raport_lunar/{luna}', 'HomeController@raportLunar')->name('raportLunar')->middleware('auth');


Route::get('/products/', 'ProductController@showDash')->name('productDash')->middleware('auth');
Route::post('/products/add', 'ProductController@add')->name('addProduct')->middleware('auth');
Route::get('/products/add_product', 'ProductController@addView')->name('addProductView')->middleware('auth');
Route::get('/products/all', 'ProductController@listView')->name('showProductView')->middleware('auth');
Route::get('/products/{id}', 'ProductController@fetchProduct')->name('fetchProduct')->middleware('auth');
Route::get('/products/edit/{id}', 'ProductController@editProduct')->name('editProduct')->middleware('auth');
Route::patch('/products/edit/{id}', 'ProductController@updateProduct')->name('updateProduct')->middleware('auth');
Route::post('/products/archive/{id}', 'ProductController@archiveProduct')->name('archiveProduct')->middleware('auth');
Route::post('/products/dearchive/{id}', 'ProductController@dearchiveProduct')->name('dearchiveProduct')->middleware('auth');
Route::post('/products/delete/{id}', 'ProductController@deleteProduct')->name('deleteProduct')->middleware('auth');
Route::post('/products/search/', 'ProductController@searchProduct')->name('searchProduct')->middleware('auth');

Route::get('/clients/', 'ClientController@showDash')->name('clientDash')->middleware('auth');
Route::get('/clients/desc', 'ClientController@showDashDesc')->name('clientDashDesc')->middleware('auth');
Route::get('/clients/asc', 'ClientController@showDashAsc')->name('clientDashAsc')->middleware('auth');
Route::get('/clients/all', 'ClientController@showClients')->name('showClients')->middleware('auth');
Route::get('/clients/add', 'ClientController@addView')->name('addClientView')->middleware('auth');
Route::post('/clients/add_client', 'ClientController@addClient')->name('addClient')->middleware('auth');
Route::post('/clients/addOnTheFly', 'ClientController@addOnTheFly')->name('addOnTheFly')->middleware('auth');
Route::post('/clients/search/', 'ClientController@searchClient')->name('searchClient')->middleware('auth');
Route::get('/clients/{id}', 'ClientController@showClient')->name('showClient')->middleware('auth');

Route::get('/orders/', 'OrderController@showDash')->name('orderDash')->middleware('auth');
Route::get('/orders/all', 'OrderController@showOrders')->name('showOrders')->middleware('auth');
Route::get('/orders/new', 'OrderController@newOrderView')->name('newOrderView')->middleware('auth');
Route::post('/orders/place', 'OrderController@placeOrder')->name('placeOrder')->middleware('auth');
Route::get('/orders/{id}/markSolved', 'OrderController@markSolved')->name('markSolved')->middleware('auth');
Route::get('/orders/{id}/delete', 'OrderController@deleteOrder')->name('deleteOrder')->middleware('auth');
Route::post('/orders/search', 'OrderController@searchDate')->name('searchOrderDate')->middleware('auth');


Route::get('/intrari/', 'IntrariController@showDash')->name('intrariDash')->middleware('auth');
Route::get('/intrari/new', 'IntrariController@addNewView')->name('addIntrareView')->middleware('auth');
Route::post('/intrari/addNew', 'IntrariController@addNew')->name('addNewIntrare')->middleware('auth');




