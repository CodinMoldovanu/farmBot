<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Client;
use App\Product;
use App\Intrare;
use DB;

class OrderController extends Controller
{
    function showDash() {
    	$orders = Order::orderBy('id', 'desc')->paginate(10);
    	$clients = Client::all();
    	$products = Product::all();
    	return view('order.show_dash', compact('orders', 'clients', 'products'));
    }
    function newOrderView() {
              $intrari = Intrare::all();
    	$existing_clients = Client::all();
    	$products = Product::where('archived', false)->get();
    	return view('order.new', compact('existing_clients', 'products', 'intrari'));
    }
    function showOrders() {
    	return view('order.list');
    }
    function markSolved($id) {
    	$order = Order::find($id);
    	$order->fulfilled = true;
    	$order->save();
    	return redirect('/orders');
    }
    function searchDate(Request $request) {
        $clients = Client::all();
        $products = Product::all();
        $orders = Order::whereDate('date', date($request->cautare))->paginate(5);
        $date = $request->cautare;
        return view('order.search_result', compact('orders', 'clients', 'products', 'date'));
    }

    function deleteOrder($id) {
        Order::destroy($id);
        return redirect('/orders');
    }



    function placeOrder(Request $request) {
    	$db_products = Product::all();
              $intrari = Intrare::all();
    	$products = $request->product;
    	// // Do some validity checks here so we can have a new Order directly.
    	$order = Order::create(['customer_id' => $request->client, 'order_amount' => $request->orderValue, 'date' => date('Y-m-d H:i:s'), ]);
    	$product_ids = collect([]);
    	$pid_array = array();
    	$profit = 0;
    	foreach ($products as $product) {
    		$exploded = explode(',', $product);
    		$pid = $exploded[0];
    		$qty = $exploded[2];
    		$val = $exploded[3];
    		$obj["id"] = $pid;
    		$obj["qty"] = $qty;
    		$pid_array[] = $obj;
    		$product_ids->push([$pid, $qty]);
    		$prod = $db_products->find($pid);
    		DB::table('order_content')->insert([
    			'order_id' => $order->id,
    			'product_id' => $pid,
    			'qty' => $qty,
    			'item_price' => $val
    			]);
            $this_product_entry = $intrari->where('produs_id', $pid)->first();
            if ($this_product_entry) {
              $this_product_entry->stoc = ($this_product_entry->stoc) - $qty;
              $this_product_entry->save(); 
            }

    		$profit = $profit + (($prod->pret_vanz - $prod->cost_prod) * $qty);
    	}
    	$order->fill([
    		'product_ids' => serialize($pid_array),
    		'profit' => $profit
    		]);
    	$order->save();
    	$client = Client::find($request->client);
    	if ($client->total_order_amount === null) {
    		    	$client->total_order_amount = $request->orderValue;
    		    	$client->save();
    	} else {
    		$client->total_order_amount = ($client->total_order_amount + ($request->orderValue));
    		$client->save();
    	}

    	return redirect('/orders');
    }
}
