<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\Order;
use App\Product;

class ClientController extends Controller
{
    function showDash() {
    	$clients = Client::orderBy('id', 'desc')->paginate(5);
    	$clientCount = Client::count();
    	$clientsWaiting = Client::where('isWaiting', '1')->count();
             $clients_over_limit = null;
        if ($cfg = DB::table('config')->first()) {
            $limit_sql = $cfg->promo_limit;
            $clients_over_limit = Client::where('total_order_amount', '>=', $limit_sql)->count();
        }

    	return view('client.dash', compact('clients', 'clientCount', 'clientsWaiting', 'clients_over_limit'));
    }

    function showDashDesc() {
    	 $clients = Client::orderBy('total_order_amount', 'desc')->paginate(5);
    	$clientCount = Client::count();
    	$clientsWaiting = Client::where('isWaiting', '1')->count();

    	return view('client.dash', compact('clients', 'clientCount', 'clientsWaiting'));
    }

    function showDashAsc() {
    	 $clients = Client::orderBy('total_order_amount', 'asc')->paginate(5);
    	$clientCount = Client::count();
    	$clientsWaiting = Client::where('isWaiting', '1')->count();

    	return view('client.dash', compact('clients', 'clientCount', 'clientsWaiting'));
    }

    function addView() {
    	return view('client.add');
    }

    function addClient(Request $request) {
    	$client = new Client;
    	$client->nume = $request->nume;
    	$client->prenume = $request->prenume;
    	$client->telephone = $request->telephone;
    	$client->email = $request->email;
    	$client->adresa = $request->adresa;
    	$client->save();
    	return redirect()->route('clientDash');
    }

    function addOnTheFly(Request $request) {
    	$client = new Client;
    	$client->nume = $request->nume;
    	$client->prenume = $request->prenume;
    	$client->telephone = $request->telephone;
    	$client->email = $request->email;
    	$client->adresa = $request->adresa;
    	$client->save();
    	return redirect()->route('newOrderView')->with('status', 'Client adăugat, îl puteți găsi în listă.');
    }

    function searchClient(Request $request) {
    	$query = $request->cautare;
    	$results = Client::where('nume', 'LIKE', $query)->orWhere('prenume', 'LIKE', $query)->orWhere('telephone', 'LIKE', $query)->paginate(5);
    	return view('client.search_results', compact('results'));
    }

    function showClient($id) {
    	$products = Product::all();
    	$client = Client::find($id);
    	$orders = Order::where('customer_id', $id)->get();
    	return view('client.show', compact('client', 'orders', 'products'));
    }
}
