<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Product;
use App\Intrare;
use App\Client;
use Barryvdh\DomPDF\Facade as PDF;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients_over_limit = null;
        if ($cfg = DB::table('config')->first()) {
            $limit_sql = $cfg->promo_limit;
            $clients_over_limit = Client::where('total_order_amount', '>=', $limit_sql)->count();
        }
        

        $delivered = Order::where('fulfilled', true)->get();
        $waiting = Order::where('fulfilled', null)->orWhere('fulfilled', false)->get();
        
        $nrwaiting = $waiting->count();
        $sum_waiting = 0;
        $sum_delivered = 0;
                foreach ($delivered as $item_delivered) {
                    $sum_delivered = $sum_delivered + $item_delivered->order_amount;
                }
           
                foreach($waiting as $item_waiting) {
                    $sum_waiting = $sum_waiting + $item_waiting->order_amount;
                }
            

        return view('home', compact('sum_waiting', 'sum_delivered', 'nrwaiting', 'clients_over_limit'));
    }

    public function raportLunar($luna) {
        $farm_name;
        if ($cfg = DB::table('config')->first()) {
            $farm_name = $cfg->company_name;
        }
        $orders = Order::whereMonth('date', $luna)->get();
        $products = Product::all();
        $clients = Client::all();
        $data = date('Y-m-d H:i:s');
        $delivered = Order::where('fulfilled', true)->get();
        $total_orders = Order::count();
        $total_orders_delivered = $delivered->count();
        $profit = 0;

                foreach ($delivered as $item_delivered) {
                    $profit = $profit + ($item_delivered->profit);
                }



        $pdf = PDF::loadView('pdf.raport_lunar', compact('orders', 'products', 'clients', 'data', 'luna', 'profit', 'total_orders', 'total_orders_delivered', 'farm_name'));
        return $pdf->stream();
    }

    public function config()
    {
        $config = DB::table('config')->orderBy('id', 'desc')->first();
        if ($config !== null) {
                    return view('config', compact('config'));
        } else {
                    return view('config');

        }
    }

    public function saveConfig(Request $request) {
        
         $config = DB::table('config');
         if (!$config->first()) {
                     $config->insert(['company_name' => $request->nume, 'promo_limit' => $request->limita]);
         } else {
            DB::table('config')
                        ->where('id', 1)
                        ->update(['company_name' => $request->nume, 'promo_limit' => $request->limita]);
         }

         return back();
    }

    public function homeAPI() 
    {   
        $products = Product::all();
        $delivered = Order::where('fulfilled', true)->get();
        $waiting = Order::where('fulfilled', null)->orWhere('fulfilled', false)->get();
        $entries = Intrare::all();
        $inStock = Intrare::where('stoc', '>', '0')->get();
        $total_orders = Order::count();
        $total_orders_delivered = $delivered->count();
        $profit = 0;

                foreach ($delivered as $item_delivered) {
                    $profit = $profit + ($item_delivered->profit);
                }
           

                return response()->json([ 'profit' => $profit, 'total_orders' => $total_orders, 'total_delivered' => $total_orders_delivered ]);

    }
}
