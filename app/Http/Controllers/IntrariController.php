<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Intrare;
use App\Product;

class IntrariController extends Controller
{
    public function showDash() {
    	$intrari = Intrare::orderBy('id', 'desc')->get();
    	$products = Product::all();
    	return view('intrari.dash', compact('intrari', 'products'));
    }

    public function addNewView() {
    	$products = Product::all();
    	return view('intrari.new', compact('products'));
    }

    public function addNew(Request $request) {
    	$intrare = new Intrare();
    	$intrare->produs_id = $request->produs;
    	$intrare->cantitate = $request->qty;
    	$intrare->stoc = $request->qty;
    	$intrare->lot = $request->lot;
    	$intrare->date = date('Y-m-d H:i:s');
    	$intrare->save();

    	return redirect('/intrari');
    }
}
