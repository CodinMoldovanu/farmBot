<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    function addView() {
    	return view('add_product');
    }

    function listView() {

    	

    	$archived_products = Product::where('archived', true)->paginate(5);
	$active_products = Product::where('archived', false)->paginate(5);
    	return view('list_product', compact('archived_products', 'active_products'));
    }

    function add(Request $request) {

    	$product = new Product;
    	$product->nume = $request->nume_prod;
    	$product->pret_vanz = $request->pret_vanz;
    	$product->cost_prod = $request->cost_prod;
    	$product->um = $request->um;
    	$product->lot = $request->lot;
    	$product->variatie = $request->mentiuni;
    	$product->save();
    	return redirect('products/all');
    }

    function fetchProduct($product_id) {
    	$product = Product::find($product_id);
    	return $product;
    }

    function updateProduct(Request $request, $id) {
    	$product = Product::find($id);
    	$product->nume = $request->nume_prod;
    	$product->pret_vanz = $request->pret_vanz;
    	$product->cost_prod = $request->cost_prod;
    	$product->um = $request->um;
    	$product->lot = $request->lot;
    	$product->variatie = $request->mentiuni;
    	$product->save();
    	return redirect('products/all');
    }

    function editProduct($product_id) {
    	$product = Product::find($product_id);
    	return view('edit_product', compact('product'));
    }
    function archiveProduct($id) {
    	$product = Product::find($id);
    	// dd($product);
    	$product->archived = true;
    	$product->save();
        return redirect('products/all');
    }
    function dearchiveProduct($id) {
    	$product = Product::find($id);
    	$product->archived = false;
    	$product->save();
        return redirect('products/all');
        // Session::flash('message', 'Task updated!');
    }

    function deleteProduct($id) {
        Product::destroy($id);
        return redirect ('products/all');
    }

    function searchProduct(Request $request) {
        $query = $request->cautare;
        $results = Product::where('nume', 'LIKE', $query)->paginate(5);
        return view('search_results', compact('results'));
    }

}
