<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    public $fillable = ['nume', 'pret_vanz', 'cost_prod', 'um', 'lot', 'variatie', 'archived'];

    public function isArchived() {
    	return $this->archived;
    }
    
}
