<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intrare extends Model
{
        protected $table = 'intrari';
        public $timestamps = false;
}
