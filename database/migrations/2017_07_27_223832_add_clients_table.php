<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->float('total_order_amount', 5, 2)->nullable();
            $table->string('nume');
            $table->string('prenume')->nullable();
            $table->string('telephone')->nullable();
            $table->string('adresa')->nullable();
            $table->string('email')->nullable();
            $table->string('orders')->nullable();
            $table->boolean('isWaiting')->default(false);
            $table->boolean('shouldReward')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
