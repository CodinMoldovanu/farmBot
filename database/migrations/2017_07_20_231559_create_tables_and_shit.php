<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesAndShit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nume');
            $table->float('pret_vanz');
            $table->float('cost_prod');
            $table->string('um');
            $table->string("lot")->nullable();
            $table->string("variatie")->nullable();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->date('date');
            $table->float('order_amount');
            // $table->string('product_ids');
            $table->boolean('fulfilled');
        });

        Schema::create('order_content', function (Blueprint $table) {
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('qty');
            $table->double('item_price');
        });

        Schema::create('intrari', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produs_id');
            $table->integer('cantitate');
            $table->string('lot');
            $table->date('date');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
