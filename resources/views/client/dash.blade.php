@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Panou Clienți</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-6 col-sm-4">
                    <h3>Aveți @if ($clientCount == 1) un client @else un număr de {{ $clientCount }} clienți @endif în baza de date. </h3>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <h3>@if ($clients_over_limit) Aveți clienți care au depășit pragul de cadou ( {{ $clients_over_limit }} ) @else Niciun client nu a atins pragul pentru cadou. @endif</h3>
                </div>
                <!-- Optional: clear the XS cols if their content doesn't match in height -->
                <div class="clearfix visible-xs-block"></div>
                <div class="col-xs-6 col-sm-4">
                    <h3> Aveți {{ $clientsWaiting }} clienți ce așteaptă comenzi. </h3>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Ultimii clienți adăugați
                <form action="{{ url('clients/search/') }}" method="POST" style="display:inline;">
                    <input type="text" class="pull-right" id="cautare" placeholder="Nume client" name="cautare" style="display:inline; width:10em;" >
                    {{ csrf_field() }}
                    <button type="submit" class="pull-right">Caută</button>
                </form>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nume Client</th>
                            <th>Prenume Client</th>
                            <th>Număr Telefon</th>
                            <th>Adresă Email</th>
                            <th>Valoare Comenzi 
                            <a href="{{ route('clientDashAsc') }}"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                            <a href="{{ route('clientDashDesc') }}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                            </th>
                            <th>Premiat</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach ($clients as $client)
                        <tr>
                            <th scope="row">{{ $client-> id }}</th>
                            <td>{{ $client->nume }}</td>
                            <td>{{ $client->prenume }}</td>
                            <td>{{ $client->telephone }}</td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->total_order_amount }}</td>
                            <td>@if ($client->rewarded && $client->rewarded_date) Da @else Nu @endif
                            <td><a href="{{ url('clients') }}/{{ $client->id }}"><button class="btn btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Vezi Client</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $clients -> links() }}
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection