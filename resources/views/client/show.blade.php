@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


            <div class="panel panel-default">
                <div class="panel-heading">Client - {{ $client->nume }} {{ $client->prenume }}</div>
                <div class="panel-body">

                <p><h4>Număr telefon: {{ $client->telefon }}</h4></p>
                <p><h4>Adresă: {{ $client->adresa }}</h4></p>
                <p><h4>Valoare Comenzi: {{ $client->total_order_amount }}</h4></p>
                <p><h4>Email: {{ $client->email }}</h4></p>

<hr>

                    Comenzile clientului 
                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Client</th>
                    <th>Data</th>
                    <th>Produse Comandă</th>
                    <th>Îndeplinit</th>
                    <th>Valoare</th>
                    <th>Profit</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $order)
                    <tr>
                    <th scope="row">{{ $order->id }}</th>
                    <td>{{ $client->nume }} {{ $client->prenume }}</td>
                    <td>{{ $order->date }}</td>

                    <td> 
                    @php 
                    $uns = $order->product_ids;
                    $prodd = unserialize($uns);
                    @endphp

                    @foreach ($prodd as $prod)
                    {{ $products->find($prod['id'])->nume }} {{ $prod['qty'] }}KG <br /> 
                    @endforeach









                    </td>

                    <td>@if ($order->fulfilled) Da @else Nu @endif</td>
                    <td>{{ $order->order_amount }}</td>
                    <td>{{ $order->profit }}</td>
                    <td>
                    @if ($order->fulfilled)
                    Livrat 
                    @else
                    <a href="{{ url('orders') }}/{{ $order->id }}/markSolved"><button class="btn btn-success" disabled><i class="fa fa-check" aria-hidden="true"></i></button></a></td>
                    @endif
                    

                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Archived product section -->

     

</div>
@endsection
