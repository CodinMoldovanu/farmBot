@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Adaugă client</div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('addClient') }}">                
                <div class="row">
                <div class="col-md-6">

                 {{ csrf_field() }}

                 <fieldset>
                     <div class="form-group">
                     <label for="nume_prod">Nume Client</label>
                     <input type="text" class="form-control" id="nume" placeholder="Numele clientului" name="nume">
                     </div>

                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="pret_vanz">Prenume Client</label>
                     <input type="text" class="form-control" id="prenume" placeholder="Prenumele clientului" name="prenume">
                     </div>            

                </div>
                </div>
                <div class="row">

                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Număr Telefon</label>
                     <input type="text" class="form-control" id="telephone" placeholder="e.g. 0703 592 121" name="telephone">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Adresă email</label>
                     <input type="text" class="form-control" id="email" placeholder="e.g. vlad.barbu@mail.ro" name="email">
                     </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-8 col-md-offset-2">
                     <div class="form-group">
                     <label for="adresa">Adresă</label>
                     <input type="text" class="form-control" id="adresa" placeholder="e.g. Aleea Speltovici, Nr.3, Bl.A, Sc.B, Parter, Ap.15" name="adresa">
                     </div>
                </div>
                </div>

  <button id="submit" name="submit" class="btn btn-primary" type="submit">Salvează</button>

                </div>

                </div>
                </fieldset> 
                </form>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
