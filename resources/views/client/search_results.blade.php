@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


            <div class="panel panel-default">
                <div class="panel-heading">@if (($results->count()) > 0)
                Căutarea a întors următoarele rezultate 
                @else 
                Căutarea nu a întors niciun rezultat
                @endif

<form action="{{ url('clients/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume client" name="cautare" style="display:inline; width:10em;" >
                     {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form>

                </div>

                <div class="panel-body">
@if (($results->count()) > 0)
                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume</th>
                    <th>Prenume</th>
                    <th>Telefon</th>
                    <th>Email</th>
                    <th>Adresă</th>
                    <th>Comenzi totale</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($results as $client)
                    <tr>
                    <th scope="row">{{ $client->id }}</th>
                    <td>{{ $client->nume }}</td>
                    <td>{{ $client->prenume }}</td>
                    <td>{{ $client->telephone }}</td>
                    <td>{{ $client->email }}</td>
                    <td>{{ $client->adresa }}</td>
                    <td>{{ $client->total_order_amount }}</td>
                    <td><a href="{{ url('clients') }}/{{ $client->id }}"><button class="btn btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Vezi Client</button></a>
</td>

                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $results -> links() }}
                </div>
            </div>
        </div>
    </div>
@endif

   

</div>
@endsection
