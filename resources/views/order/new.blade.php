@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
        <!-- col-md-offset-2 above -->
        @if (session('status'))
<div class="alert alert-success" role="alert">{{ session('status') }}</div>
@endif
            <div class="panel panel-default">
                <div class="panel-heading">Înregistrare comandă</div>

                <div class="panel-body">
                <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-group">
  <label for="select_client_type">Alegeți Client</label>
  <select class="form-control js-example-basic-single" id="select_client_type" onchange="changed()">
    <option value="Client Nou">Client Nou</option>
@foreach ($existing_clients as $client)
<option id="{{ $client->id }}" value="{{ $client->nume }} {{ $client->prenume }}">{{ $client->nume }} {{ $client->prenume }}</option>
@endforeach
  </select>
</div>
</div>
                </div>

                    <form method="POST" action="{{ route('addOnTheFly') }}" id="newClientForm">                
                <div class="row">
                <div class="col-md-6">

                 {{ csrf_field() }}

                 <fieldset>
                     <div class="form-group">
                     <label for="nume_prod">Nume Client</label>
                     <input type="text" class="form-control" id="nume" placeholder="Numele clientului" name="nume">
                     </div>

                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="pret_vanz">Prenume Client</label>
                     <input type="text" class="form-control" id="prenume" placeholder="Prenumele clientului" name="prenume">
                     </div>            

                </div>
                </div>
                <div class="row">

                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Număr Telefon</label>
                     <input type="text" class="form-control" id="telephone" placeholder="e.g. 0703 592 121" name="telephone">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Adresă email</label>
                     <input type="text" class="form-control" id="email" placeholder="e.g. vlad.barbu@mail.ro" name="email">
                     </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-8 col-md-offset-2">
                     <div class="form-group">
                     <label for="adresa">Adresă</label>
                     <input type="text" class="form-control" id="adresa" placeholder="e.g. Aleea Speltovici, Nr.3, Bl.A, Sc.B, Parter, Ap.15" name="adresa">
                     </div>
                </div>
                </div>

  <button id="submit" name="submit" class="btn btn-primary" type="submit">Salvează</button>

                </div>
                </fieldset> 
                </form>
                </div>

                </div>
                <div class="col-md-4"> <!-- Second Panel -->
            <div class="panel panel-default">
                <div class="panel-heading">Comanda</div>

                <div class="panel-body" id="finalOrder">
<form method="POST" action="{{ route('placeOrder') }}" id="placeOrderForm">
{{ csrf_field() }}
<input type="hidden" name="client" value="" id="clientInput">
<input type="hidden" name="orderValue" value="" id="orderValue">

<table class="table table-bordered table-condensed" id="finalOrderTableParent">
                    <thead>
                    <tr>
                    <th>Nume Produs</th>
                    <th>Cantitate</th>
                    <th>Preț</th>
                    <th>X</th>
                    </tr>
                    </thead>
                    
                    <tbody id="finalOrderTable">

                    </tbody>
                    </table>
                    <hr />
                    <span class="product-big">Preț Total</span> <span class="product-big pull-right" id="totalComanda"></span><br />
                    <button type="submit" class="btn btn-success">Trimite</button>
                    </form>

                </div>
                </div>



                </div>
                </div>
 <div class="row">
    <div class="col-md-12">

<div class="panel panel-default">
                <div class="panel-heading">Add Order</div>

                <div class="panel-body">

                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Produs</th>
                    <th>Preț Vânzare</th>
                    <th>Cost Producție</th>
                    <th>UM</th>
                    <th>Lot</th>
                    <th>Cantitate în Stoc</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($products as $product)
                    <tr>
                    <th scope="row">{{ $product->id }}</th>
                    <td class="product-big" id="nume{{ $product->id }}">{{ $product->nume }}</td>
                    <td class="product-big" id="pret{{ $product->id }}">{{ $product->pret_vanz }}</td>
                    <td class="product-big">{{ $product->cost_prod }}</td>
                    <td class="product-big" id="um{{ $product->id }}">{{ $product->um }}</td>
                    <td class="product-big">{{ $product->lot }}</td>
                    <td>
                        <!-- Cantitate -->
                        

                        <input type="text" class="form-control" id="cantitate{{ $product->id }}" placeholder="{{ $intrari->find($product->id)->stoc or 'Neintrat' }}" name="cantitate[]" size="5" onChange="">
                    </td>
                    <td> 
<!-- Changed to Button -->
      <button class="btn btn-info" onClick="toggleAdded({{ $product->id }})">Adaugă</button>
</td>

                    </tr>
                    @endforeach
                    </tbody>
                    </table>

                </div>
                </div>

    </div>
            </div>

        </div>



    </div>
   
    </div>
</div>
<style>
.product-big {
    font-size: 1.25em;
}
</style>
<script>

 $(document).ready(function() {
  $(".js-example-basic-single").select2();
});



class OrderItem {
  constructor($name, $qty, $value, $um, $id) {
    this.name = $name;
    this.qty = $qty;
    this.value = $value;
    this.um = $um;
     let tr = document.createElement("tr");
  tr.setAttribute("id", "orderItem" + $name);
  let nume = document.createElement("td");
  let cantitate = document.createElement("td");
  let pret = document.createElement("td");
  let sterge = document.createElement("td");
  nume.innerText = $name;
  cantitate.innerText = $qty + $um;
  pret.innerText = $value;
  sterge.innerHTML = '<span onclick="removeItem(&quot;orderItem' + nume.innerText + '&quot;)"><i class="fa fa-times" aria-hidden="true"></i></span>';
  let inputField = document.createElement("input");
  inputField.type = "hidden";
  inputField.name = "product[]";
  let array = [$id, $name, $qty, $value];
  inputField.value = array;
  // "["+ [$id, $name, $qty] + "]";
  inputField.id = "inputField" + $id;

    tr.appendChild(nume);
    tr.appendChild(cantitate);
    tr.appendChild(pret);
    tr.appendChild(sterge);
    tr.appendChild(inputField);
        return tr;
     }
 
}


function changed() {
    let select = document.getElementById("select_client_type");
    let form = document.getElementById("newClientForm");
    let clientInput = document.getElementById("clientInput");
        if (select.value !== ("Client Nou")) {
        form.setAttribute("style", "display: none");
    }
    if (select.value == ("Client Nou")) {
        form.setAttribute("style", "");
    }
    clientInput.value = select.options[select.selectedIndex].id;

}

function removeItem($rowName) {
    let row = document.getElementById($rowName);
    row.parentNode.removeChild(row);
    totalValue();
}
</script>
<script>
function toggleAdded($id) {
// check if toggled
let checkbox = document.getElementById("checkbox" + $id);
let qty = document.getElementById("cantitate" + $id);
let price = document.getElementById("pret" + $id);
let nume = document.getElementById("nume" + $id);
let um = document.getElementById("um" + $id);
let finalOrder = document.getElementById("finalOrder");
let finalOrderTable = document.getElementById("finalOrderTable");
    let value = qty.value * price.innerText;
if (qty.value > 0) {
    finalOrderTable.appendChild(new OrderItem(nume.innerText, qty.value, value.toFixed(2), um.innerText, $id));
    totalValue();
    qty.value = "";
}

        // }
}

function totalValue() {
    let orderTable = document.getElementById("finalOrderTableParent");
    let totalComanda = document.getElementById("totalComanda");
    let sum = 0;
    for (let i = 1, row; row = orderTable.rows[i]; i++) {
        sum = sum + parseFloat(row.cells[2].innerText); 
    }
    totalComanda.innerText=sum.toFixed(2) + "RON";
    let orderValue = document.getElementById("orderValue");
    orderValue.value = sum.toFixed(2);
}
</script>

@endsection
