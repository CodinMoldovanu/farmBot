@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


                    <div class="panel panel-default">
                <div class="panel-heading">Panou comenzi

<!-- <form action="{{ url('products/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume produs" name="cautare" style="display:inline; width:10em;" >
                                          {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form> -->

                </div>

                <div class="panel-body">
<form action="" method="POST">
Căutare după client
<input type="text" id="cautare" name="cautare">
{{ csrf_field() }}
<button type="submit">Caută</button>
</form> <br />
<form action="{{ route('searchOrderDate') }}" method="POST">
Căutare după dată
<input type="date" id="cautare" name="cautare">
{{ csrf_field() }}
<button type="submit">Caută</button>
</form>

                </div>
                </div>
                </div>
                </div>



            <div class="panel panel-default">
                <div class="panel-heading">Listă Comenzi

<form action="{{ url('products/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume produs" name="cautare" style="display:inline; width:10em;" >
                                          {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form>

                </div>

                <div class="panel-body">

                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Client</th>
                    <th>Data</th>
                    <th>Produse Comandă</th>
                    <th>Îndeplinit</th>
                    <th>Valoare</th>
                    <th>Profit</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($orders as $order)
                    <tr>
                    <th scope="row">{{ $order->id }}</th>
                    <td>

                    <a href="{{ url('clients') }}/{{ $clients->find($order->customer_id)->id }}">
                    {{ $clients->find($order->customer_id)->nume }} {{ $clients->find($order->customer_id)->prenume }} 
                    </a>



                    </td>
                    <td>{{ $order->date }}</td>

                    <td> 
                                            @php 
                    $uns = $order->product_ids;
                    $prodd = unserialize($uns);
                    @endphp

                    @if (is_array($prodd))
                    @foreach ($prodd as $prod)
                    {{ $products->find($prod['id'])->nume }} {{ $prod['qty'] }}KG <br /> 
                    @endforeach
                    @endif
                    </td>

                    <td>@if ($order->fulfilled) Da @else Nu @endif</td>
                    <td>{{ $order->order_amount }}</td>
                    <td>{{ $order->profit }}</td>
                    <td>
                    @if ($order->fulfilled)
                    Livrat 
                    @else
                    <a href="{{ url('orders') }}/{{ $order->id }}/markSolved"><button class="btn btn-success" disabled><i class="fa fa-check" aria-hidden="true"></i></button></a><a href="{{ url('orders') }}/{{ $order->id }}/delete"><button class="btn btn-danger" disabled><i class="fa fa-trash" aria-hidden="true"></i></button></a>     </td>
                    @endif
              
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>

    <!-- Archived product section -->

     

</div>
@endsection
