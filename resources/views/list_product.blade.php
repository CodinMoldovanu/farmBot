@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


            <div class="panel panel-default">
                <div class="panel-heading">Lista produse 

<form action="{{ url('products/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume produs" name="cautare" style="display:inline; width:10em;" >
                                          {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form>

                </div>

                <div class="panel-body">

                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Produs</th>
                    <th>Preț Vânzare</th>
                    <th>Cost Producție</th>
                    <th>UM</th>
                    <th>Lot</th>
                    <th>Mențiuni</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($active_products as $product)
                    <tr>
                    <th scope="row">{{ $product-> id }}</th>
                    <td>{{ $product->nume }}</td>
                    <td>{{ $product->pret_vanz }}</td>
                    <td>{{ $product->cost_prod }}</td>
                    <td>{{ $product->um }}</td>
                    <td>{{ $product->lot }}</td>
                    <td>{{ $product->variatie }}</td>
                    <td><a href="{{ url('products/edit') }}/{{ $product->id }}"><button class="btn btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Modifică</button></a>
</td>

                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $active_products -> links() }}
                </div>
            </div>
        </div>
    </div>

    <!-- Archived product section -->

     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Produse Arhivate</div>

                <div class="panel-body">
                    <!-- Modal here. -->
                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              ...
                            </div>
                          </div>
                        </div>

                    <!-- End of Modal -->
                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Produs</th>
                    <th>Preț Vânzare</th>
                    <th>Cost Producție</th>
                    <th>UM</th>
                    <th>Lot</th>
                    <th>Mențiuni</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($archived_products as $archived_product)
                    <tr>
                    <th scope="row">{{ $archived_product-> id }}</th>
                    <td>{{ $archived_product->nume }}</td>
                    <td>{{ $archived_product->pret_vanz }}</td>
                    <td>{{ $archived_product->cost_prod }}</td>
                    <td>{{ $archived_product->um }}</td>
                    <td>{{ $archived_product->lot }}</td>
                    <td>{{ $archived_product->variatie }}</td>
                    <td><a href="{{ url('products/edit') }}/{{ $archived_product->id }}"><button class="btn btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Modifică</button></a>
</td>

                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $archived_products -> links() }}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
