@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Adaugă produs</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('products/add') }}">                
                <div class="row">
                <div class="col-md-6">

                 {{ csrf_field() }}

                 <fieldset>
                     <div class="form-group">
                     <label for="nume_prod">Nume Produs</label>
                     <input type="text" class="form-control" id="nume_prod" placeholder="Numele produsului" name="nume_prod">
                     </div>

                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="pret_vanz">Prețul de vânzare al produsului</label>
                     <input type="text" class="form-control" id="pret_vanz" placeholder="Prețul produsului cu punct pentru separație decimală" name="pret_vanz">
                     </div>            

                </div>
                </div>
                <div class="row">

                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Costul de producție</label>
                     <input type="text" class="form-control" id="cost_prod" placeholder="Costul de producție al produsului" name="cost_prod">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="um">UM</label>
                     <select class="form-control" id="um"  name="um">
                     <option>KG</option>
                     <option>BUC</option>
                     </select>
                     </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="lot">Lot</label>
                     <input type="text" class="form-control" id="lot" placeholder="Identificare lot" name="lot">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="mentiuni">Mențiuni</label>
                     <input type="text" class="form-control" id="mentiuni" placeholder="Alte mențiuni" name="mentiuni">
                     </div>
                </div>
                </div>
  <button id="submit" name="submit" class="btn btn-primary" type="submit">Salvează</button>

                </div>

                </div>
                </fieldset> 
                </form>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
