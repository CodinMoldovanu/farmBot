@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">Configurare
                </div>
                <div class="panel-body">
                <form action="{{ url('/config/save') }}" method="POST">
                 {{ csrf_field() }}
                    <fieldset>
                     <div class="form-group">
                     <label for="nume">Nume Companie</label>
                     <input type="text" class="form-control" id="nume" placeholder="numele fermei/firmei" name="nume" value="{{ $config->company_name or '' }}">
                     </div>
                      <div class="form-group">
                     <label for="limita">Limita de premiere</label>
                     <input type="text" class="form-control" id="limita" placeholder="e.g. '5000'" name="limita" value="{{ $config->promo_limit or '' }}">
                     </div>
                       <button id="submit" name="submit" class="btn btn-primary" type="submit">Salvează</button>
                       </fieldset>
                </form>
<br />
<!--     <p><a href="#"><button class="btn btn-info">Raport lună</button></a>
    <a href="#"><button class="btn btn-info">Raport an</button></a>
    <a href="#"><button class="btn btn-info">Raport personalizat</button></a></p> -->




                


                </div>
            </div>
        </div>
    </div>


</div>
@endsection
