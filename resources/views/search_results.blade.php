@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


            <div class="panel panel-default">
                <div class="panel-heading">@if (($results->count()) > 0)
                Căutarea a întors următoarele rezultate 
                @else 
                Căutarea nu a întors niciun rezultat
                @endif

<form action="{{ url('products/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume produs" name="cautare" style="display:inline; width:10em;" >
                     {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form>

                </div>

                <div class="panel-body">
@if (($results->count()) > 0)
                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nume Produs</th>
                    <th>Preț Vânzare</th>
                    <th>Cost Producție</th>
                    <th>UM</th>
                    <th>Lot</th>
                    <th>Mențiuni</th>
                    <th>Acțiuni</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($results as $product)
                    <tr>
                    <th scope="row">{{ $product-> id }}</th>
                    <td>{{ $product->nume }}</td>
                    <td>{{ $product->pret_vanz }}</td>
                    <td>{{ $product->cost_prod }}</td>
                    <td>{{ $product->um }}</td>
                    <td>{{ $product->lot }}</td>
                    <td>{{ $product->variatie }}</td>
                    <td><a href="{{ url('products/edit') }}/{{ $product->id }}"><button class="btn btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Modifică</button></a>
</td>

                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $results -> links() }}
                </div>
            </div>
        </div>
    </div>
@endif

   

</div>
@endsection
