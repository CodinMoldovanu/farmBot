<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">

<div class="pure-g">
    <div class="pure-u-1-3"><p>{{ $farm_name or 'Numele nu a fost setat' }} <br />
    			      Generat la {{ $data }}
    			      Pentru luna {{ $luna }}</p>
    </div>
    <div class="pure-u-1-3"></div>
    <div class="pure-u-1-3"></div>
</div>
<hr>
	<div class="pure-g">
	<div class="pure-u-1-1">

<!-- Main content -->

<table class="pure-table pure-table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Client</th>
            <th>Data</th>
            <th>Comanda</th>
            <th>Total</th>
            <th>Profit</th>
        </tr>
       	

    </thead>

    <tbody>
    @php
    	$items_per_page = 10;
    	$items_so_far = 0;
    @endphp
    @foreach ($orders as $order)
        <tr>
            <td>{{ $order->id }}</td>
            <td> {{ $clients->find($order->customer_id)->nume }} {{ $clients->find($order->customer_id)->prenume }}</td>
            <td>{{ $order->date }}</td>
            <td>@php 
                    $uns = $order->product_ids;
                    $prodd = unserialize($uns);
                    @endphp

                    @if (is_array($prodd))
                    @foreach ($prodd as $prod)
                    {{ $products->find($prod['id'])->nume }} {{ $prod['qty'] }}KG <br /> 
                    @endforeach
                    @endif
             </td>
            <td>{{ $order->order_amount }}RON</td>
            <td>{{ $order->profit }}RON</td>

        </tr>
    @php
    	$items_so_far ++;
    @endphp
    @if ((($items_so_far % 10) == 0) &&($items_so_far >1))
    <div class="new-page"></div>
    @endif
        @endforeach

    </tbody>
</table>


<!-- /Main content -->

	</div>
	</div>



<link href="https://fonts.googleapis.com/css?family=Noto+Serif&amp;subset=latin-ext" rel="stylesheet">
<style>
td { font-family: 'Noto Serif', serif; font-size: 1px; }
</style>
<style>
hr {
	border-top: 1px solid #8c8b8b;
}
.new-page {
    page-break-after: always;
}
</style>