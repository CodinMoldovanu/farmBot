@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


            <div class="panel panel-default">
                <div class="panel-heading">Listă intrări

<!-- <form action="{{ url('products/search/') }}" method="POST" style="display:inline;">
                     <input type="text" class="pull-right" id="cautare" placeholder="Nume produs" name="cautare" style="display:inline; width:10em;" >
                                          {{ csrf_field() }}
                     <button type="submit" class="pull-right">Caută</button>
</form> -->

                </div>

                <div class="panel-body">

                    <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Produs</th>
                    <th>Cantitate</th>
                    <th>Lot</th>
                    <th>Data</th>
                    <th>Stoc</th>

                    </tr>
                    </thead>
                    
                    <tbody>
                    @foreach ($intrari as $intrare)
                    <tr>
                    <th scope="row">{{ $intrare->id }}</th>
                    <td>{{ $products->find($intrare->produs_id)->nume }}</td>
                    <td>{{ $intrare->cantitate }} {{ $products->find($intrare->produs_id)->um }}</td>
                    <td>{{ $intrare->lot }}</td>
                    <td>{{ $intrare->date }}</td>
                    <td>{{ $intrare->stoc }}</td>
                    <td>
                        

                        
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Archived product section -->

     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Produse Arsadhivate</div>

                <div class="panel-body">
                    <!-- Modal here. -->
                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              ...
                            </div>
                          </div>
                        </div>

                    <!-- End of Modal -->
                

                </div>
            </div>
        </div>
    </div>

</div>
@endsection
