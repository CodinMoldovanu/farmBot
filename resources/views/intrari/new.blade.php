@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modifică</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('intrari/addNew') }}" style="display: inline;">        
     <!-- <input type="hidden" name="_method" value="PATCH"> -->
                <div class="row">
                <div class="col-md-6">

                 {{ csrf_field() }}

                 <fieldset>
                     <div class="form-group">
                     <label for="produs">Alegeți produs</label>
                     <select class="form-control js-example-basic-single" id="produs" name="produs" onchange="">
                @foreach ($products as $product)
               <option id="{{ $product->id }}" value="{{ $product->id }}">{{ $product->nume }} {{ $product->um }}</option>
               @endforeach
                                </select>
                                    </div>

                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="qty">Cantitate</label>
                     <input type="text" class="form-control" id="qty" value="" name="qty">
                     </div>            

                </div>
                </div>
                <div class="row">

                <div class="col-md-8 col-md-offset-2">
                     <div class="form-group">
                     <label for="lot">Lot</label>
                     <input type="text" class="form-control" id="lot" value="" name="lot">
                     </div>
                </div>

                </div>
  <button id="submit" name="submit" class="btn btn-primary" type="submit" style="display:inline-block;">Salvează</button></form>


 <form action="" method="POST" style="display: inline;">
  {{ csrf_field() }}
      <button type="submit" name="submit" class="btn btn-danger" style="display:inline-block;">
      Șterge Produs</button></form>
                </div>

                </div>
                </fieldset> 
                
                </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
 $(document).ready(function() {
  $(".js-example-basic-single").select2();
});
</script>
@endsection
