@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modifică</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('products/edit') }}/{{ $product->id }}" style="display: inline;">        
     <input type="hidden" name="_method" value="PATCH">
                <div class="row">
                <div class="col-md-6">

                 {{ csrf_field() }}

                 <fieldset>
                     <div class="form-group">
                     <label for="nume_prod">Nume Produs</label>
                     <input type="text" class="form-control" id="nume_prod" value="{{ $product->nume }}" name="nume_prod">
                     </div>

                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="pret_vanz">Prețul de vânzare al produsului</label>
                     <input type="text" class="form-control" id="pret_vanz" value="{{ $product->pret_vanz }}" name="pret_vanz">
                     </div>            

                </div>
                </div>
                <div class="row">

                <div class="col-md-6">
                     <div class="form-group">
                     <label for="cost_prod">Costul de producție</label>
                     <input type="text" class="form-control" id="cost_prod" value="{{ $product->cost_prod }}" name="cost_prod">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="um">UM</label>
                     <select class="form-control" id="um"  name="um" value="{{ $product->um }}">
                     <option>KG</option>
                     <option>BUC</option>
                     </select>
                     </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="lot">Lot</label>
                     <input type="text" class="form-control" id="lot" placeholder="Identificare lot" name="lot" value="{{ $product->lot }}">
                     </div>
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                     <label for="mentiuni">Mențiuni</label>
                     <input type="text" class="form-control" id="mentiuni" placeholder="Alte mențiuni" name="mentiuni" value="{{ $product->variatie }}">
                     </div>
                </div>
                </div>
  <button id="submit" name="submit" class="btn btn-primary" type="submit" style="display:inline-block;">Salvează</button></form>
  @if (($product->archived) == 1)
  <form action="{{ url('/products/dearchive') }}/{{ $product->id }}" method="POST" style="display: inline;">
  {{ csrf_field() }}
      <button type="submit" name="submit" class="btn btn-success" style="display:inline-block;">
      Dezarhivează</button></form>
        @else
 <form action="{{ url('/products/archive') }}/{{ $product->id }}" method="POST" style="display: inline;">
  {{ csrf_field() }}
      <button type="submit" name="submit" class="btn btn-info" style="display:inline-block;">
      Arhivează</button></form>        @endif

 <form action="{{ url('/products/delete') }}/{{ $product->id }}" method="POST" style="display: inline;">
  {{ csrf_field() }}
      <button type="submit" name="submit" class="btn btn-danger" style="display:inline-block;">
      Șterge Produs</button></form>
                </div>

                </div>
                </fieldset> 
                
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
