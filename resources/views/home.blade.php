@extends('layouts.app')
@section('content')
<div class="container" id="body">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Panou farmBot</h3>
                </div>
                <div class="panel-body">
                <div class="loader" id="spinner"></div>
                <div class="loading-content" style="display: none" id="home-content">
                    <div class="col--6 col-sm-4">
                        <h4>Profit: <h3 id="profit"></h3></h4>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <h4>Număr comenzi: <h3 id="nr_comenzi"></h3></h4>
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                    <div class="col-xs-6 col-sm-4">
                        <h4>Număr comenzi livrate: <h3 id="nr_livrate"></h3></h4>
                    </div>
                    <br /> 
                    @if ($clients_over_limit)
                    <hr>
                    <h4> Aveți clienți care au ajuns la pragul de premiere ({{ $clients_over_limit }})</h4>
                    @endif
                    <!-- new row -->
                    <!-- <div class="col--6 col-sm-4">
                        <h4>Profit: <h3 id="profit"></h3></h4>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <h4>Valoare totală intrări: <h3 id="total_entry_value"></h3></h4>
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                    <div class="col-xs-6 col-sm-4">
                        <h4>Valoare stocuri: <h3 id="total_stock_value"></h3></h4>
                    </div> -->
                    </div> <!-- loading content -->
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">

<style>

.loader,
.loader:after {
  border-radius: 50%;
  width: 10em;
  height: 10em;
}
.loader {
  margin: 60px auto;
  font-size: 10px;
  position: relative;
  text-indent: -9999em;
  border-top: 1.1em solid rgba(62,204,189, 0.2);
  border-right: 1.1em solid rgba(62,204,189, 0.2);
  border-bottom: 1.1em solid rgba(62,204,189, 0.2);
  border-left: 1.1em solid #3eccbd;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: load8 1.1s infinite linear;
  animation: load8 1.1s infinite linear;
}
@-webkit-keyframes load8 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load8 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}


</style>

<script>


$(document).ready(function() {
let spinner = document.getElementById("spinner");
let home = document.getElementById("home-content");
let profit = document.getElementById("profit");
let comenzi = document.getElementById("nr_comenzi");
let livrate = document.getElementById("nr_livrate");
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
var req = $.get("/api/home", function(data) {
    profit.innerText = data.profit + "RON";
    comenzi.innerText = data.total_orders;
    livrate.innerText = parseInt(data.total_delivered);
    spinner.setAttribute("style", "display: none");
    home.setAttribute("style", "");
});



});
</script>
@endsection